package com.java_8_training.examples.data_parallelism;

import com.java_8_training.answers.lambdas.Apple;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class Scratch {
    public static void main(String[] args) throws Exception {
        List<Apple> apples = new ArrayList<>(Arrays.asList(
                new Apple(10, "red"),
                new Apple(10, "green"),
                new Apple(20, "red")));

        // Average weight of red apples?
        Predicate<Apple> isRed = apple -> apple.getColor().equals("red");

        final double averageWeightOfRedApples = apples.parallelStream()
                .filter(isRed)
                .mapToInt(Apple::getWeight)
                .average()
                .getAsDouble();

        System.out.println(averageWeightOfRedApples);
    }
}
