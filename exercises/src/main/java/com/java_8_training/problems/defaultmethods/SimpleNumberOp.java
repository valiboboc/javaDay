package com.java_8_training.problems.defaultmethods;

public interface SimpleNumberOp {

    int getValue();

    static int add(SimpleNumberOp lhs, SimpleNumberOp rhs) {
        return lhs.getValue() + rhs.getValue();
    }

    default void getValueAsDouble(){
        throw new UnsupportedOperationException();
    }
}
