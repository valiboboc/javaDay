package com.java_8_training.problems.defaultmethods;

/**
 * Created by vboboc on 3/22/2017.
 */
public class ABBA implements A,B {
    @Override
    public String printHello() {
        return A.super.printHello();
    }
}
