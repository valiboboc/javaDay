package com.java_8_training.problems.datetime;

import java.time.Duration;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class BirthdayDiary {

    private Map<String, LocalDate> birthdays;

    public BirthdayDiary() {
        birthdays = new HashMap<>();
    }

    public LocalDate addBirthday(String name, int day, int month, int year) {
        LocalDate birthday = LocalDate.of(year, month, day);
        birthdays.put(name,birthday);
        return birthday;
    }

    public LocalDate getBirthdayFor(String name) {
        return birthdays.get(name);
    }

    public int getAgeInYear(String name, int year) {
        LocalDate startDateInclusive = birthdays.get(name);
        return Period.between(startDateInclusive, LocalDate.of(year,startDateInclusive.getMonth(),startDateInclusive.getDayOfMonth())).getYears();
    }

    public Set<String> getFriendsOfAgeIn(int age, int year) {
        return birthdays.entrySet().stream().filter(stringLocalDateEntry ->
                age == Period.between(stringLocalDateEntry.getValue(), LocalDate.of(year,stringLocalDateEntry.getValue().getMonth(),stringLocalDateEntry.getValue().getDayOfMonth())).getYears())
                .map(stringLocalDateEntry -> stringLocalDateEntry.getKey()).collect(Collectors.toSet())
                ;
    }

    public Set<String> getBirthdaysIn(Month month) {
        return birthdays.entrySet().stream().filter(stringLocalDateEntry -> month.equals(stringLocalDateEntry.getValue().getMonth())).map(stringLocalDateEntry -> stringLocalDateEntry.getKey()).collect(Collectors.toSet());
    }

    public int getTotalAgeInYears() {
        return birthdays.values().stream().mapToInt(stringLocalDateEntry -> Period.between(stringLocalDateEntry,LocalDate.now()).getYears()).sum();
    }
}