package com.java_8_training.problems.design;

import java.util.function.Supplier;

// TODO: refactor to use deferred execution pattern
public class MiniAssert {

    public static void assertFalse(boolean condition, Supplier<String> message) {
        if (condition) {
            fail(message.get());
        }
    }

    public static void assertSame(Object expected, Object actual,  Supplier<String> message) {
        if (expected != actual) {
            fail(message.get());
        }
    }

    public static void fail(String message) {
        if (message == null) {
            throw new AssertionError();
        }
        throw new AssertionError(message);
    }
}
